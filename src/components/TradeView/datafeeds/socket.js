class socket {
  constructor(url = 'wss://localhost:8080/api/v1/exchange/ticker/10001', options) {
    this.heartBeatTimer = null
    this.options = options
    this.messageMap = {}
    this.connState = 0
    this.socket = null
    this.url = url
  }
  doOpen() {
    console.log("3-1");
    if (this.connState) return
    this.connState = 1
    this.afterOpenEmit = []
    const socket = new WebSocket(this.url)
    socket.binaryType = 'arraybuffer'
    socket.onopen = evt => this.onOpen(evt)
    socket.onclose = evt => this.onClose(evt)
    socket.onmessage = evt => this.onMessage(evt.data)
    socket.onerror = err => this.onError(err)
    this.socket = socket
  }
  on(name, handler) {
    console.log("3-2");
    this.messageMap[name] = handler
  }
  onOpen(evt) {
    console.log("3-3");
    this.connState = 2
    this.onReceiver({ Event: 'open' })
  }
  onReceiver(data) {
    console.log("3-4");
    const callback = this.messageMap[data.Event]
    if (callback) callback(data.Data)
  }
  send(data) {
    console.log("3-5");
    console.log("■socket.send■" + JSON.stringify(data));
    this.socket.send(JSON.stringify(data))
  }
  checkOpen() {
    console.log("3-6");
    return this.connState === 2
  }
  onClose() {
    console.log("3-004");
    this.connState = 0
    if (this.connState) {
      this.onReceiver({ Event: 'close' })
    }
  }
  onMessage(message) {
    console.log("3-6");
    try {
      const data = JSON.parse(message)
      this.onReceiver({ Event: 'message', Data: data })
    } catch (err) {
      console.error(' >> Data parsing error:', err)
    }
  }
  onError(err) {
  }
  doClose() {
    this.socket.close()
  }
  destroy() {
    if (this.heartBeatTimer) {
      clearInterval(this.heartBeatTimer)
      this.heartBeatTimer = null
    }
    this.doClose()
    this.messageMap = {}
    this.connState = 0
    this.socket = null
  }
}

export default socket