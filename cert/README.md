# localhost用自己署名証明書について

# 使い方
cert/localhost-certificates.p12をダブルクリックで開く。
パスワードはなし。パスワードダイアログで何も入力せずエンターを押す。
Finder - Application - Utilities - Keychain Accessを開き、loginの所に「localhost」が追加された事を確認する。

# オプション 自己署名証明書を一から作成する手順
## 自己署名証明書の作成
- Keychain Accessを起動。Finder - Applications - Utilities - Keychain Access

- メニューからKeychain Access - Certificate Assistant - Create a Certificate選択

- 画像の様に順に設定する
![1](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/1.png)

![2](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/2.png)

![3](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/3.png)

![4](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/4.png)

![5](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/5.png)

![6](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/6.png)

![7](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/7.png)

![8](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/8.png)

![9](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/9.png)

- 最後にCreateボタンを押す
![10](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/10.png)

- Keychain Accessの「login」に証明書が生成された事を確認
![11](https://bitbucket.org/zcomwallet/front/raw/self-signed-cert/cert/imgs/11.png)

- localhostの証明書アイコンを選択して右クリック、Export "localhost"を選択
![12](imgs/12.png)

- p12形式を選択し、デスクトップに保存

- *.p12 形式から*.pemへ変換。webpack-dev-serverはpem形式しか使えないため。
```
$ openssl pkcs12 -in ~/Desktop/Certificates.p12 -out server.pem
Enter Import Password:<-そのままENTER
MAC verified OK
Enter PEM pass phrase:<-そのままENTER
```

- server.pemを確認
```
$ cat ~/Desktop/server.pem
```
