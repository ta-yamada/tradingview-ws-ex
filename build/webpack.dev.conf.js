'use strict'
const utils = require('./utils')
const webpack = require('webpack')
const config = require('../config')
const merge = require('webpack-merge')
const path = require('path')
const baseWebpackConfig = require('./webpack.base.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const portfinder = require('portfinder')

const HOST = process.env.HOST
const PORT = process.env.PORT && Number(process.env.PORT)

// const stagingServer = 'staging2.zcomwallet.com';
const stagingServer = 'coin.z.com';

// const pushServer = 'push2.zcomwallet.com';
const pushServer = 'push.coin.z.com';

const devWebpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap, usePostCSS: true })
  },
  // cheap-module-eval-source-map is faster for development
  devtool: config.dev.devtool,

  // these devServer options should be customized in /config/index.js
  devServer: {
    clientLogLevel: 'warning',
    hot: true,
    contentBase: false, // since we use CopyWebpackPlugin.
    compress: true,
    host: HOST || config.dev.host,
    port: PORT || config.dev.port,
    open: config.dev.autoOpenBrowser,
    overlay: config.dev.errorOverlay
      ? { warnings: false, errors: true }
      : false,
    publicPath: config.dev.assetsPublicPath,
    quiet: true, // necessary for FriendlyErrorsPlugin
    watchOptions: {
      poll: config.dev.poll,
    },
    proxy: {
      '/api/v1/defs/message.json': {
        target: `https://${stagingServer}`,
        // target: 'http://localhost:10010',
        secure: false,
        headers: {
          Origin: `https://${stagingServer}`,
        },
        changeOrigin: true,
      },
      '/api/v1': {
        target: `https://${stagingServer}`,
        //target: 'http://localhost:10010',
        secure: false,
        headers: {
          Origin: `https://${stagingServer}`,
        },
        changeOrigin: true,
        cookieDomainRewrite: 'localhost',
      },
      '/api/v1/push': {
        target: `wss://${pushServer}`,
        ws: true,
        secure: false,
        // Note:
        // {changeOrigin: true} not worked. Only work on named virtual host?
        // Instead, put 'Origin: <staging server name>' into headers.
        headers: {
          Origin: `https://${stagingServer}`,
        },
      },
      '/api/v1/exchange/ticker': {
        target: `wss://${pushServer}`,
        ws: true,
        secure: false,
        headers: {
          Host: `${pushServer}`,
          Origin: `https://${stagingServer}`,
        },
      },
      '/api/v1/exchange/orderbooks': {
        target: `wss://${pushServer}`,
        ws: true,
        secure: false,
        headers: {
          Host: `${pushServer}`,
          Origin: `https://${stagingServer}`,
        },
      },
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': require('../config/dev.env')
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
    new webpack.NoEmitOnErrorsPlugin(),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      inject: true
    }),
    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: config.dev.assetsSubDirectory,
        ignore: ['.*']
      }
    ])
  ]
})

module.exports = new Promise((resolve, reject) => {
  portfinder.basePort = process.env.PORT || config.dev.port
  portfinder.getPort((err, port) => {
    if (err) {
      reject(err)
    } else {
      // publish the new Port, necessary for e2e tests
      process.env.PORT = port
      // add port to devServer config
      devWebpackConfig.devServer.port = port

      // Add FriendlyErrorsPlugin
      devWebpackConfig.plugins.push(new FriendlyErrorsPlugin({
        compilationSuccessInfo: {
          messages: [`Your application is running here: https://${devWebpackConfig.devServer.host}:${port}`],
        },
        onErrors: config.dev.notifyOnErrors
          ? utils.createNotifierCallback()
          : undefined
      }))

      resolve(devWebpackConfig)
    }
  })
})
