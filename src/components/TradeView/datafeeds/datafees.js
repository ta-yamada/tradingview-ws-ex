/**
 * JS API
 */
import dataUpdater from './dataUpdater'
class datafeeds {

  /**
   * JS API
   * @param {*Object} vue 
   */
  constructor(vue) {
    this.self = vue
    this.barsUpdater = new dataUpdater(this)
  }

  /**
   * @param {*Function} callback  
   * `onReady` should return result asynchronously.
   */
  onReady(callback) {
    console.log("1-1");
    return new Promise((resolve, reject) => {
      let configuration = this.defaultConfiguration()
      if (this.self.getConfig) {
        configuration = Object.assign(this.defaultConfiguration(), this.self.getConfig())
      }
      resolve(configuration)
    }).then(data => callback(data))
  }

  /**
   * @param {*String} symbolName  シンボル名
   * @param {*Function} onSymbolResolvedCallback 成功
   * @param {*Function} onResolveErrorCallback   失败
   * `resolveSymbol` should return result asynchronously.
   */
  resolveSymbol(symbolName, onSymbolResolvedCallback, onResolveErrorCallback) {
    console.log("1-2");
    return new Promise((resolve, reject) => {
      let symbolInfo = this.defaultSymbol()
      if (this.self.getSymbol) {
        symbolInfo = Object.assign(this.defaultSymbol(), this.self.getSymbol())
      }
      resolve(symbolInfo)
    }).then(data => onSymbolResolvedCallback(data)).catch(err => onResolveErrorCallback(err))
  }

  /**
   * @param {*Object} symbolInfo  SymbolInfoオブジェクト
   * @param {*String} resolution  足
   * @param {*Number} rangeStartDate  unixタイムスタンプ、一番左に必要なバー時間
   * @param {*Number} rangeEndDate  unixタイムスタンプ、右端に必要なバー時間
   * @param {*Function} onDataCallback  
   * @param {*Function} onErrorCallback  
   */
  getBars(symbolInfo, resolution, rangeStartDate, rangeEndDate, onDataCallback, onErrorCallback) {
    console.log("1-3");
    console.log("rangeStartDate=" + rangeStartDate);
    console.log("rangeEndDate=" + rangeEndDate);
    const onLoadedCallback = data => {
      data && data.length ? onDataCallback(data, { noData: true }) : onDataCallback([], { noData: true })
    }
    this.self.getBars(symbolInfo, resolution, rangeStartDate, rangeEndDate, onLoadedCallback)
  }

  /**
   * Charting Libraryは、シンボルのリアルタイム更新を受信するときにこの関数を呼び出します。図書館では、onRealtimeCallback最新のバーを更新するか、新しいバーを追加するたびに呼び出すことを前提としています。
   * @param {*Object} symbolInfo シンボル
   * @param {*String} resolution 足
   * @param {*Function} onRealtimeCallback  
   * @param {*String} subscriberUID subscriberUID
   * @param {*Function} onResetCacheNeededCallback （バージョン1.7以降）：barデータが変更されたときに実行されるfunction（）
   */
  subscribeBars(symbolInfo, resolution, onRealtimeCallback, subscriberUID, onResetCacheNeededCallback) {
    console.log("1-4");
    this.barsUpdater.subscribeBars(symbolInfo, resolution, onRealtimeCallback, subscriberUID, onResetCacheNeededCallback)
  }

  /**
   * Charting Libraryは、このサブスクライバの更新をもう受信したくない場合にこの関数を呼び出します。subscriberUIDライブラリがsubscribeBars前に渡されたのと同じオブジェクトになります。
   * @param {*String} subscriberUID subscriberUID
   */
  unsubscribeBars(subscriberUID) {
    this.barsUpdater.unsubscribeBars(subscriberUID)
  }

  /**
   * 初期値
   */
  defaultConfiguration() {
    return {
      supports_search: true,
      supports_group_request: false,
      supported_resolutions: ['1', '5', '15', '30', '60', '1D', '2D', '3D', '1W', '1M'],
      supports_marks: true,
      supports_timescale_marks: true,
      supports_time: true
    }
  }

  /**
   * 初期値
   */
  defaultSymbol() {
    return {
      'name': 'BTCUSDT',
      'timezone': 'Asia/Tokyo',
      'minmov': 1,
      'minmov2': 0,
      'pointvalue': 1,
      'fractional': false,
      'session': '24x7',
      'has_intraday': true,
      'has_no_volume': false,
      'description': 'BTCUSDT',
      'pricescale': 1,
      'ticker': 'BTCUSDT',
      'supported_resolutions': ['1', '5', '15', '30', '60', '1D', '2D', '3D', '1W', '1M']
    }
  }
}

export default datafeeds